package com.example.recyclerviewapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NameClickListener {

    private static final String TAG = "MainActivity";
    private static final int LAUNCH_SECOND_ACTIVITY = 0;

    private RecyclerView recyclerView;
    private List<String> dataList;
    private CustomAdapter customAdapter;
    public Button button;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupData();
        setupRecyclerView();

        button = findViewById(R.id.btn_new_activity);


        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startSecondActivity();
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupRecyclerView();
    }

    private void startSecondActivity(){
        int LAUNCH_SECOND_ACTIVITY = 1;
        Intent i = new Intent(this, Second_activity.class);
        startActivityForResult(i, LAUNCH_SECOND_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK){
            String str=data.getStringExtra("result");
            dataList.add(str);
            customAdapter.notifyDataSetChanged();
        }
        if (resultCode != Activity.RESULT_OK) {
            dataList.add("It's bad");
            customAdapter.notifyDataSetChanged();
        }

    }//onActivityResult

    private void setupData() {
        dataList = new ArrayList<>();
        dataList.add("Jedan");
        dataList.add("Dva");
        dataList.add("Tri");
    }

    private void setupRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        customAdapter = new CustomAdapter(dataList, this);
        recyclerView.setAdapter(customAdapter);
    }


    @Override
    public void onNameClick(int position) {
        dataList.remove(position);
        customAdapter.notifyDataSetChanged();
        Log.d(TAG, " onNameClick position " + position);
    }
}

