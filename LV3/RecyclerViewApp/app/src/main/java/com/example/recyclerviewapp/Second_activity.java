package com.example.recyclerviewapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

public class Second_activity extends AppCompatActivity {
    public Button button;
    public EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_activity);
        button = findViewById(R.id.btn_add);
        editText = findViewById((R.id.editTextName));


        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String stringToPassBack = editText.getText().toString();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", stringToPassBack);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }


}