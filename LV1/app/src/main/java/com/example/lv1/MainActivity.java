package com.example.lv1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    String mTitle[] = {"David Beckham", "LeBron James", "Arturo Vidal"};
    String mYears[] = {"1975.", "1984.", "1987."};
    String mDesciption[] = {"Direktor.", "Kralj.", "Mesar"};
    int images[] = {R.drawable.Beckham, R.drawable.LeBron, R.drawable.Vidal};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        MyAdapter adapter = new MyAdapter(this, mTitle, mYears, mDesciption, images);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    Toast.makeText(MainActivity.this, "Barcelona", Toast.LENGTH_LONG).show();
                }
                if (position == 1){
                    Toast.makeText(MainActivity.this, "Ferrari", Toast.LENGTH_LONG).show();
                }
                if (position == 2){
                    Toast.makeText(MainActivity.this, "Chicago Bulls", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String rTitle[];
        String rYears[];
        String rDesciption[];
        int rImages[];

        MyAdapter(Context c, String title[], String years[], String description[], int imgs[]){
            super(c, R.layout.row, R.id.textView1, title);
            this.context = c;
            this.rTitle = title;
            this.rYears = years;
            this.rDesciption = description;
            this.rImages = imgs;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myTitle = row.findViewById(R.id.textView1);
            TextView myYears = row.findViewById(R.id.textView2);
            TextView myDescription = row.findViewById(R.id.textView3);

            images.setImageResource(rImages[position]);
            myTitle.setText(rTitle[position]);
            myYears.setText(rYears[position]);
            myDescription.setText(rDesciption[position]);


            return row;
        }
    }
}
