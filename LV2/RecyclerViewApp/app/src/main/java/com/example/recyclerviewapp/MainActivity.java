package com.example.recyclerviewapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NameClickListener {

    private static final String TAG = "MainActivity";

    private RecyclerView recyclerView;
    private List<String> dataList;
    private CustomAdapter customAdapter;
    public Button button;
    public EditText editText;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupData();
        setupRecyclerView();

        button = findViewById(R.id.btn_add);
        editText = findViewById((R.id.editTextName));

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataList.add(editText.getText().toString());
                customAdapter.notifyDataSetChanged();
            }
        });

    }

    private void setupData() {
        dataList = new ArrayList<>();
        dataList.add("Branka");
        dataList.add("Marija");
        dataList.add("Kruno");

    }

    private void setupRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        customAdapter = new CustomAdapter(dataList, this);
        recyclerView.setAdapter(customAdapter);
    }


    @Override
    public void onNameClick(int position) {
        dataList.remove(position);
        customAdapter.notifyDataSetChanged();
        Log.d(TAG, " onNameClick position " + position);
    }
}

